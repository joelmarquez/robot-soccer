#ifndef Strategy_H
#define Strategy_H

#include <stdio.h>
#include <time.h>

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the STRATEGY_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// STRATEGY_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef STRATEGY_EXPORTS
#define STRATEGY_API __declspec(dllexport)
#else
#define STRATEGY_API __declspec(dllimport)
#endif

const long PLAYERS_PER_SIDE = 5;

#define Distancia(x,y,xf,yf) ( sqrt((xf-x)*(xf-x) + (yf-y)*(yf-y)) )
const double PI = 3.1415923;
#define RAD2DEG (180.0/PI)
#define DEG2RAD (PI/180.0)
#define Rad2Deg(a) ((double)a * RAD2DEG)
#define Deg2Rad(a) ((double)a * DEG2RAD)

#define ATENUAR 0.5

#define MaxVel	125

//Definición de constantes de la cancha
const int AreaGL_XDer = 22 ;
const int AreaGR_XIzq = 79 ;
const int AreaG_YSup = 56 ;
const int AreaG_YInf = 25 ;

const int AreaCL_XDer = 14 ;
const int AreaCR_XIzq = 86 ;
const int AreaC_YSup = 49 ;
const int AreaC_YInf = 32 ;

const int Cancha_XDer = 92 ;
const int Cancha_XIzq = 8 ;
const int Cancha_YSup = 76 ;
const int Cancha_YInf = 7 ;
const int Cancha_XCentro = 50 ;
const int Cancha_YCentro = 40 ;

typedef struct
{
	double x, y, z;
} Vector3D;

typedef struct
{
	long left, right, top, bottom;
} Bounds;

typedef struct
{
	Vector3D pos;
	double rotation;
	double velocityLeft, velocityRight;
} Robot;

typedef struct
{
	Vector3D pos;
	double rotation;
} OpponentRobot;

typedef struct
{
	Vector3D pos;
} Ball;

typedef struct
{
	Vector3D mid, sup, inf, tope;
} Goal;

typedef struct
{
	Robot home[PLAYERS_PER_SIDE];
	OpponentRobot opponent[PLAYERS_PER_SIDE];
	Ball currentBall, lastBall, predictedBall;
	Bounds fieldBounds, goalBounds;
	long gameState;
	long whosBall;
	void *userData;
	char *display;

} Environment;

typedef void (*MyStrategyProc)(Environment*);

/*util.h*/
void PredictBall ( Environment *env );
void PlayNormal(Environment *env );
void Arquero( Robot *robot, Environment *env );
void Jugador( Robot *robot, Environment *env, bool masCerca);
void IrA(Robot *m_prJugador,double dXDestino, double dYDestino, double dVelMax); 
double CalcularAnguloAGirar(double x0,double y0, double xf, double yf, double rr); 
double Deg2Rad2(double dAngGrados);
double Rad2Deg2(double dAngRad);
double NormalizarAngulo(double angulo);
double CalcularAngulo2Pts(double x0, double y0, double xf, double yf); 
int Signo(double n);
void Girar(Robot *m_prJugador,double anguloAGirar);
double DistanciaRobotPelota(Robot *m_prJugador, Environment *env);


////////////////
void PlayPenalty( Environment *env );
void GoToMiddleField(Robot *robot);
void GoToPenaltyZone(Robot *robot);

void Defensor(Robot *robot, Environment *env);
void Delantero(Robot *robot, Environment *env);
void Delantero2(Robot *robot, Environment *env);

//Funciones para patear penal
void Patea_Penal(Robot *robot, Environment *env);
void Penal_B( Environment *env ); 
void Penal_Y( Environment *env ); 

Vector3D PosicionDeTiro(Robot *robot, Environment *env);

char myMessage[200]; //big enough???

extern "C" STRATEGY_API void Create ( Environment *env ); // implement this function to allocate user data and assign to Environment->userData
extern "C" STRATEGY_API void Strategy ( Environment *env );
extern "C" STRATEGY_API void Destroy ( Environment *env ); // implement this function to free user data created in  Create (Environment*)


#endif // Strategy_H