// Strategy.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "Strategy.h"
#include <math.h>
#include <iostream>
#include <fstream>
using namespace std;

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}


// gameState
const long PLAY = 0;	// Partido
const long FREE_BALL = 1;
const long PLACE_KICK = 2;
const long PENALTY_KICK = 3;
const long FREE_KICK = 4;
const long GOAL_KICK = 5;

// whosBall
const long ANYONES_BALL = 0;
const long BLUE_BALL = 1;
const long YELLOW_BALL = 2;

// Medidas nuestras
const Vector3D ARCO_AZUL = { 92, 40, 0 };
const Vector3D ARCO_AMARILLO = { 8, 40, 0 };

const double AREA_AMARILLA = 22;
const double AREA_AZUL = 79;

// Defensa
const int VelMax=125;

extern "C" STRATEGY_API void Create ( Environment *env ) { }

// --------------------------------------------------------------------------------------------

extern "C" STRATEGY_API void Destroy ( Environment *env ) { }

// --------------------------------------------------------------------------------------------

extern "C" STRATEGY_API void Strategy ( Environment *env )
{
	PredictBall ( env );

	switch (env->gameState)
	{
	case PLAY:
		PlayNormal(env);
		break;
	case FREE_BALL:
		PlayNormal(env);
		break;
	case PLACE_KICK:
		PlayNormal(env);
		break;
	case PENALTY_KICK:
		switch (env->whosBall)
		{
		case ANYONES_BALL:
			PlayNormal(env);
			break;
		case BLUE_BALL:
			Penal_B(env);
			break;
		case YELLOW_BALL:
			Penal_Y(env);
			break;
		}
		break;
	case FREE_KICK:
		switch (env->whosBall)
		{
		case ANYONES_BALL:
			PlayNormal(env);
			break;
		case BLUE_BALL:
			PlayNormal(env);
			break;
		case YELLOW_BALL:
			PlayNormal(env);
			break;
		}
		break;
	case GOAL_KICK:
		switch (env->whosBall)
		{
		case ANYONES_BALL:
			PlayNormal(env);
			break;
		case BLUE_BALL:
			PlayNormal(env);
			break;
		case YELLOW_BALL:
			PlayNormal(env);
			break;
		}
		break;
	}
	
	sprintf(myMessage, "PredictBall-x:%f ! PredictedBall-y:%f",env->predictedBall.pos.x,env->predictedBall.pos.y);
	env->display = myMessage;
}

//------------------------------------------------------------------------------------------------

void PredictBall ( Environment *env )
{
	double dx = env->currentBall.pos.x - env->lastBall.pos.x;
	double dy = env->currentBall.pos.y - env->lastBall.pos.y;
	env->predictedBall.pos.x = env->currentBall.pos.x + dx;
	env->predictedBall.pos.y = env->currentBall.pos.y + dy;
}

//------------------------------------------------------------------------------------------------

double ObtenerAngulo( double x0, double y0, double xf, double yf )
{
	double dx, dy, r, alfa;

	dx = xf-x0;
	dy = yf-y0;

	if (dx == 0 && dy == 0)
		alfa = 0;
	else
	{
		if (dx == 0)
		{
			if (dy > 0)
				alfa = 90;
			else
				alfa = -90;
		}
		else
			if (dy == 0)
			{
				if (dx > 0)
					alfa = 0;
				else
					alfa = 180;
			}
			else
			{
				// dx y dy != 0
				if (dx > 0)
					r = atan(fabs(dy) / fabs(dx));
				else
					r = atan(fabs(dx) / fabs(dy));
				alfa = Rad2Deg(r);
				if(dx < 0)
					alfa += 90;
				if (dy < 0)
					alfa *= -1;
			}
	}

	return alfa;
}

// =====================================================================
// J U G A D A S   E S P E C I A L E S		//K27
// =====================================================================

void PlayNormal( Environment *env )
{	
	int masCerca,i;
	double distMin,dist;

	masCerca = 1;
	distMin = Distancia(env->home[1].pos.x,env->home[1].pos.y,fabs(env->currentBall.pos.x), fabs(env->currentBall.pos.y));
	for(i=2; i<5; i++)
	{
		dist = Distancia(env->home[i].pos.x,env->home[i].pos.y,fabs(env->currentBall.pos.x), fabs(env->currentBall.pos.y));
	
		if (dist < distMin)
		{
			masCerca = i;
			distMin = dist;
		}
	}

	Arquero( &env->home[0], env);
	Defensor(&env->home[1], env);
	Defensor(&env->home[2], env);
	Delantero(&env->home[3], env);
	Delantero(&env->home[4], env);
}

//Penal para el equipo amarillo
void Penal_Y( Environment *env )
{	
	//Elegimos al arquero para patear el penal
	Patea_Penal( &env->home[0], env);

}

//Penal para el equipo azul
void Penal_B( Environment *env )
{	
	Arquero( &env->home[0], env);

}
// Funcion principal de los movimientos del arquero 35 a 48,5 ARCO
void Arquero(Robot *robot, Environment *env)
{
	if(env->predictedBall.pos.y<AreaG_YInf)
		IrA(robot,7,AreaG_YInf,VelMax);	
	else
		if(env->predictedBall.pos.y>AreaG_YSup)
			IrA(robot,7,AreaG_YSup,VelMax);
		else		
			IrA(robot,7,env->predictedBall.pos.y,VelMax);
}

void Defensor(Robot *robot, Environment *env) 
{
	if(env->predictedBall.pos.x >= AreaGL_XDer && env->predictedBall.pos.x <= 100)
	{		
		if(env->predictedBall.pos.y < AreaG_YInf)
			IrA(robot,AreaGL_XDer,AreaC_YInf,MaxVel); 
		
		if(env->predictedBall.pos.y > AreaG_YInf && env->predictedBall.pos.y < AreaG_YSup)
			IrA(robot,AreaGL_XDer,env->predictedBall.pos.y,MaxVel);
		
		if(env->predictedBall.pos.y > AreaG_YSup) 
			IrA(robot,AreaGL_XDer,AreaC_YSup,MaxVel);
	}
	else
	{
		if(env->predictedBall.pos.y >= AreaG_YSup)
			IrA(robot,env->predictedBall.pos.x,AreaG_YSup,MaxVel);
		else if(env->predictedBall.pos.y <=AreaG_YInf)
			IrA(robot,env->predictedBall.pos.x,AreaG_YInf,MaxVel);
	}
}

void Delantero(Robot *robot, Environment *env) 
{
	if(robot->pos.x < env->currentBall.pos.x)
	{		
		Vector3D tiro = PosicionDeTiro(robot, env);
		IrA(robot,tiro.x,tiro.y,MaxVel);
	}
	else
	{
		IrA(robot,env->currentBall.pos.x - 20,env->currentBall.pos.y,MaxVel);
	}
}

void Delantero2(Robot *robot, Environment *env) 
{
	if(robot->pos.x < env->currentBall.pos.x)
	{		
		IrA(robot,env->currentBall.pos.x,env->currentBall.pos.y,MaxVel);
	}
	else
	{
		IrA(robot,env->currentBall.pos.x - 20,env->currentBall.pos.y,MaxVel);
	}
}

void Patea_Penal(Robot *robot, Environment *env) 
{	
	Vector3D tiro = PosicionDeTiro(robot, env);
	int Desvio_tiro = 0;

	IrA(robot,tiro.x,tiro.y+Desvio_tiro,MaxVel);

}
// Devuelve el �ngulo a girar por un jugador para queda mirando a la posici�n xf yf.
double CalcularAnguloAGirar(double x0, double y0, double xf, double yf, double rr)
{
	
		double Ang2Puntos = CalcularAngulo2Pts(x0, y0, xf, yf);
		double AngAGirar = Ang2Puntos - NormalizarAngulo(rr);
	
		return Deg2Rad2(AngAGirar);
}

double CalcularAngulo2Pts(double xo, double yo, double xf, double yf)
{
	double dx, dy, alfa, constAngle = 0;
	dx = xf - xo;
	dy = yf - yo;
	if (dx == 0 && dy == 0) {
		alfa = 0;
	}
	else if (dx == 0) {
		if (dy > 0)
			alfa = 90;
		else
			alfa = 270;
	}
	else if (dy == 0) {
		if (dx > 0)
			alfa = 0;
		else
			alfa = 180;
	}
	else {
		alfa = atan(fabs(dy) / fabs(dx));
		alfa = Rad2Deg2(alfa);
		
		if ((dx > 0) && (dy > 0))
			alfa = alfa;
		else if ((dx < 0) && (dy > 0))
			alfa = 180 - alfa;
		else if ((dx < 0) && (dy < 0))
			alfa = 180 + alfa;
		else
			alfa = 360 - alfa;
	}
	return alfa;
}
int Signo(double n)
{
	return (int)(n == 0 ? 0 : (n / fabs(n)));
}

void IrA(Robot *m_prJugador,double dXDestino, double dYDestino, double dVelMax) 
{	
	double dAngAGirar = CalcularAnguloAGirar(m_prJugador->pos.x, m_prJugador->pos.y, 
							dXDestino, dYDestino, m_prJugador->rotation);
	
	double t = ((cos(dAngAGirar) * cos(dAngAGirar)) * Signo(cos(dAngAGirar)));
	double r = ((sin(dAngAGirar) * sin(dAngAGirar)) * Signo(sin(dAngAGirar)));
	
	double dVelIzq = dVelMax * (t - r);
	double dVelDer = dVelMax * (t + r);
	double dif = (dVelIzq - dVelDer);
	double d = fabs(dif / (dVelMax * 2.0));
	double f = (1 - d) + 0.11;
	if ((dVelIzq * f) < 0) {
		m_prJugador->velocityLeft = dVelDer * f;
		m_prJugador->velocityRight = dVelIzq * f;
	}
	else {
		m_prJugador->velocityLeft = dVelIzq * f;
		m_prJugador->velocityRight = dVelDer * f;
	}
}

void Girar(Robot *m_prJugador,double anguloAGirar) 
{
	double vi, vd;
	if (fabs(anguloAGirar) > 150) {
		vi = 100;
		vd = -100;
	}
	else if (fabs(anguloAGirar) > 100) {
		vi = 75;
		vd = -75;
	}
	else if (fabs(anguloAGirar) > 70) {
		vi = 50;
		vd = -50;
	}
	else if (fabs(anguloAGirar) > 40) {
		vi = 20;
		vd = -20;
	}
	else if (fabs(anguloAGirar) > 20) {
		vi = 7;
		vd = -7;
	}
	else {
		vi = 4;
		vd = -4;
	}
	if (anguloAGirar > 0) {
		vi = (-1) * vi;
		vd = (-1) * vd;
	}
	
	m_prJugador->velocityLeft = vi;
	m_prJugador->velocityRight = vd;
}

//-------------------------------------------------------------------------------------------
// Deveulve el �ngulo en grados pasado como par�metro en radianes.
double Deg2Rad2(double dAngGrados)
{
	return dAngGrados * (3.14 / 180.0);
}

//-------------------------------------------------------------------------------------------
// Deveulve el �ngulo en radianes pasado como par�metro en grados.
double Rad2Deg2(double dAngRad)
{
	return dAngRad * (180.0 / 3.14);
}

double NormalizarAngulo(double angulo)
{
	while (angulo >  360) angulo -= 360;
	while (angulo < -360) angulo += 360;
	if (angulo < 0) angulo += 360;
	return angulo;
}

double DistanciaRobotPelota(Robot *m_prJugador, Environment *env)
{
	return Distancia(m_prJugador->pos.x,m_prJugador->pos.y,fabs(env->currentBall.pos.x), fabs(env->currentBall.pos.y));
}

Vector3D PosicionDeTiro(Robot *robot, Environment *env)
{
	Vector3D result;
	double ponderador = .02;

	double xRobot = robot->pos.x;
	double xPelota = env->currentBall.pos.x;

	double xPelotaArco = ARCO_AZUL.x - xPelota;
	double xPond = xPelotaArco * ponderador;
	double xTiro = xPelota - xPond;

	// Si est� m�s cerca de la pelota que la distancia a la que se tiene que poner, que vaya a la pelota
	result.x = xTiro > xRobot ? xTiro : xPelota;

	double yRobot = robot->pos.y;
	double yPelota = env->currentBall.pos.y;

	double yPelotaArco = yPelota - ARCO_AZUL.y;
	double yPond = yPelotaArco * ponderador;
	double yTiro = yPelota + yPond;

	if (yPond >= -0.001 && yPond <= 0.001)
		result.y = yPelota;

	if (yPelotaArco >= 0) {
		if (yRobot >= yPelota && yRobot <= yTiro)
			result.y = yPelota;
		else
			result.y = yTiro;
	}
	else {
		if (yRobot >= yTiro && yRobot <= yPelota)
			result.y = yPelota;
		else
			result.y = yTiro;
	}

	return result;
}